#!/bin/bash
set -o nounset
set -o errexit

# Read configuration values from $gitlab_rotate_backup_config or /etc/gitlab-rotate-backup.conf
. ${gitlab_rotate_backup_config-/etc/gitlab-rotate-backup.conf}
progname="$0"

function rotate
{
  unmount_backup
  exit_unless_backup_file_exists
  destroy_old_snapshots
  create_new_snapshot
  mount_latest_snapshot
}

function purge
{
  unmount_backup
  destroy_old_snapshots
}

function unmount_backup
{
  if mountpoint -q ${backup_mountpoint} ; then
    abort_if_failed umount ${backup_mountpoint}
  fi
}

function exit_unless_backup_file_exists
{
  if ! [[ -e "${backup_timestamp_file}" ]] ; then
    exit 0
  fi
}

function destroy_old_snapshots
{
  for snapshot in $(existing_snapshots); do
    abort_if_failed lvremove --force ${snapshot}
  done
}

function existing_snapshots
{
  lvs --noheadings 2>/dev/null | awk "/${snapshot_prefix}[0-9]+/ { print \$2 \"/\" \$1 }"
}

function create_new_snapshot
{
  abort_if_failed touch ${backup_timestamp_file}
  sleep 5 # wait for touch to propagate through DRBD
  abort_if_failed lvcreate --extents "${snapshot_extents}" --name $(date "+${snapshot_prefix}%s") --snapshot ${backup_source_lv}
}

function mount_latest_snapshot
{
  local latest_snapshot=$(existing_snapshots | tail -1)
  if [[ -z "${latest_snapshot}" ]] ; then
    error_echo "${progname}: could not find a snapshot to mount"
    exit 1
  fi
  abort_if_failed mount -o ro /dev/${latest_snapshot} ${backup_mountpoint}
}

function abort_if_failed
{
  local output
  if ! output=$("$@" 2>&1) ; then
    error_echo "${progname}: command failed: $@"
    error_echo "${output}"
    exit 1
  fi
}

function error_echo
{
  echo "$@" 1>&2
}

cmd=${1:-}
case ${cmd} in
  rotate)
    rotate
    exit 0
    ;;
  purge)
    purge
    exit 0
    ;;
  *)
    echo "Usage: $0 (rotate|purge)"
    exit 1
esac

exit 1
