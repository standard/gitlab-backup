#!/bin/sh
#
# Copyright 2015 GitLab B.V.
#

# AWS_DEFAULT_OUTPUT is required for parsing the output of 'aws' with AWK etc.
export AWS_DEFAULT_OUTPUT='text'

# Read configuration values from $gitlab_ebs_snapshot_config or /etc/gitlab-ebs-snapshot.conf
. ${gitlab_ebs_snapshot_config:-/etc/gitlab-ebs-snapshot.conf}


main() {
  # Allow overriding the instance whose volumes we are snapshotting via the
  # environment
  if [ -z "${instance}" ] ; then
    instance=$(get_instance)
  fi

  volumes=$(get_volumes "${instance}")

  if [ -z "${volumes}" ] ; then
    echo Failed to find attached volumes
    exit 1
  fi

  description="gitlab-ebs-snapshot backup $(date '+%Y-%m-%d %H:%M UTC%z')"

  # Commands after 'freeze_fs' must be guaranteed not to take too long
  freeze_fs

  for vol in ${volumes}; do
    # Give each snapshot 5 seconds to complete; other wise it gets 'kill -9'
    with_timeout aws ec2 create-snapshot --description "${description}" --volume-id ${vol}
  done

  unfreeze_fs
  notify_snitch
}

with_timeout() {
  timeout --signal=9 5 "$@"
}

get_instance() {
  with_timeout ec2metadata | awk '/^instance-id:/ { print $2 }'
}

get_volumes() {
  with_timeout aws ec2 describe-volumes --filter "Name=attachment.instance-id,Values=${1}" \
    | awk '/ATTACHMENTS/ { print $NF}'
}

freeze_fs() {
  if [ -n "${freeze}" ] ; then
    echo "Freezing ${freeze}"
    fsfreeze -f "${freeze}"
  fi
}

unfreeze_fs() {
  if [ -n "${freeze}" ] ; then
    fsfreeze -u "${freeze}"
    echo "Unfroze ${freeze}"
  fi
}

notify_snitch() {
  if [ -n "${snitch_url}" ] ; then
    with_timeout curl "${snitch_url}"
  fi
}

main
