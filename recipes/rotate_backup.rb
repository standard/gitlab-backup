script_path = '/opt/gitlab-backup/bin/gitlab-rotate-backup'

directory File.dirname(script_path) do
  recursive true
end

cookbook_file 'gitlab-rotate-backup' do
  path script_path
  mode 0755
end

backup_mountpoint = node['gitlab-backup']['snapshot_mountpoint']

directory backup_mountpoint do
  recursive true
end

template '/etc/gitlab-rotate-backup.conf' do
  variables node['gitlab-backup']['rotate_backup'].to_hash.merge(
    :backup_mountpoint => backup_mountpoint
  )
end

cron 'rotate_backup' do
  weekday node['gitlab-backup']['rotate_backup']['cron_weekday']
  hour node['gitlab-backup']['rotate_backup']['cron_hour']
  minute node['gitlab-backup']['rotate_backup']['cron_minute']
  path node['gitlab-backup']['rotate_backup']['cron_path']
  command "#{script_path} rotate"
end

purge_task = 'gitlab-rotate-backup-purge.conf'
cookbook_file purge_task do
  path File.join('/etc/init', purge_task)
end
