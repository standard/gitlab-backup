#
# Cookbook Name:: gitlab-backup
# Recipe:: default
#
# Copyright (C) 2014 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'gitlab-backup::rsync_server'
include_recipe 'gitlab-backup::rotate_backup'
