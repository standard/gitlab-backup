chef_gem 'chef-vault'
require 'chef-vault'
ebs_snapshot_conf = GitLab::AttributesWithSecrets.get(node, 'gitlab-backup', 'ebs-snapshot')

# Install dependencies
%w{python2.7 python-pip python-virtualenv}.each do |pkg|
  package pkg
end

install_dir = '/opt/gitlab-backup/ebs-snapshot'
directory install_dir do
  recursive true
end

execute "virtualenv #{install_dir}" do
  creates File.join(install_dir, 'bin/python')
end

requirements_txt = File.join(install_dir, 'requirements.txt')
cookbook_file requirements_txt do
  source 'ebs-snapshot-requirements.txt'
end

pip = File.join(install_dir, 'bin/pip')
execute "#{pip} install -r #{requirements_txt}" do
  not_if "#{pip} freeze | cmp -- #{requirements_txt} -"
end

# Create gitlab-ebs-snapshot script
script_path = '/opt/gitlab-backup/bin/gitlab-ebs-snapshot'

directory File.dirname(script_path) do
  recursive true
end

cookbook_file script_path do
  mode '0755'
end

# Render /etc/gitlab-ebs-snapshot.conf, using attributes and secrets
template '/etc/gitlab-ebs-snapshot.conf' do
  mode '0600'
  variables ebs_snapshot_conf.to_hash
end

cron 'gitlab-ebs-snapshot' do
  weekday ebs_snapshot_conf['cron_weekday']
  hour ebs_snapshot_conf['cron_hour']
  minute ebs_snapshot_conf['cron_minute']
  path ebs_snapshot_conf['cron_path']
  command script_path
end
