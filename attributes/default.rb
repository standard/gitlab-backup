default['gitlab-backup']['snapshot_mountpoint'] = "/mnt/gitlab_backup"
default['gitlab-backup']['backup_client_ip'] = nil

default['gitlab-backup']['rotate_backup']['backup_source_lv'] = "gitlab_vg/drbd"
default['gitlab-backup']['rotate_backup']['backup_timestamp_file'] = "/var/opt/gitlab/backup.txt"
default['gitlab-backup']['rotate_backup']['snapshot_prefix'] = "gitlab_auto_snapshot_"
default['gitlab-backup']['rotate_backup']['snapshot_extents'] = "50%FREE"
default['gitlab-backup']['rotate_backup']['cron_weekday'] = "*"
default['gitlab-backup']['rotate_backup']['cron_hour'] = "*/8"
default['gitlab-backup']['rotate_backup']['cron_minute'] = "0"
default['gitlab-backup']['rotate_backup']['cron_path'] = "/usr/bin:/bin:/sbin"

default['gitlab-backup']['pull_backup']['rsync_servers'] = []
default['gitlab-backup']['pull_backup']['rsync_path'] = "gitlab-backup/"
default['gitlab-backup']['pull_backup']['backup_dataset'] = "gitlab_backup/gitlab_data"
default['gitlab-backup']['pull_backup']['local_backup_mountpoint'] = "/gitlab_backup/gitlab_data"
default['gitlab-backup']['pull_backup']['backup_timestamp_file'] = "backup.txt"
default['gitlab-backup']['pull_backup']['snitch_url'] = nil
default['gitlab-backup']['pull_backup']['cron_weekday'] = "*"
default['gitlab-backup']['pull_backup']['cron_hour'] = "*/8"
default['gitlab-backup']['pull_backup']['cron_minute'] = "15"
default['gitlab-backup']['pull_backup']['cron_path'] = "/usr/bin:/bin:/sbin"
default['gitlab-backup']['pull_backup']['data_bag'] = nil

default['gitlab-backup']['restore_backup']['mount_path'] = '/gitlab_backup/restore_data'
default['gitlab-backup']['restore_backup']['user'] = 'backup-restore'

default['gitlab-backup']['ebs-snapshot']['chef_vault'] = nil
default['gitlab-backup']['ebs-snapshot']['chef_vault_item'] = nil
default['gitlab-backup']['ebs-snapshot']['freeze'] = '/var/opt/gitlab'
default['gitlab-backup']['ebs-snapshot']['cron_weekday'] = "0"
default['gitlab-backup']['ebs-snapshot']['cron_hour'] = "10"
default['gitlab-backup']['ebs-snapshot']['cron_minute'] = "0"
default['gitlab-backup']['ebs-snapshot']['cron_path'] = '/opt/gitlab-backup/ebs-snapshot/bin:/usr/local/sbin:/usr/sbin:/sbin:/usr/local/bin:/usr/bin:/bin'
