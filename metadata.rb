name             'gitlab-backup'
maintainer       'GitLab B.V.'
maintainer_email 'jacob@gitlab.com'
license          'All rights reserved'
description      'Configure Rsync daemon for GitLab backups'
long_description 'Installs/Configures gitlab-backup'
version          '0.2.5'

depends 'rsync'
